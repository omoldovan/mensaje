import json, httplib2, datetime, google
from oauth2client import client
from apiclient import discovery
from flask import Flask, render_template, redirect, url_for, session, request, Response
from google.appengine.ext import ndb
from time import mktime

app = Flask(__name__)
app.secret_key = 'A0Zr98j/3yX R~XHH!jmN]LWX/,?RT'

class Letter(ndb.Model):
    author = ndb.StringProperty()
    title = ndb.StringProperty()
    text = ndb.StringProperty()
    location = ndb.GeoPtProperty()
    date = ndb.DateTimeProperty(auto_now_add=True)

class TimeEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, datetime.datetime):
            return int(mktime(obj.timetuple()))
        elif isinstance(obj, google.appengine.api.datastore_types.GeoPt):
            return {'lat': obj.lat, 'lon': obj.lon}
        return json.JSONEncoder.default(self, obj)

@app.route('/')
def index():
    if 'credentials' not in session:
        return redirect(url_for('oauth2callback'))
        credentials = client.OAuth2Credentials.from_json(session['credentials'])

    credentials = client.OAuth2Credentials.from_json(session['credentials'])
    if credentials.access_token_expired:
        return redirect(url_for('oauth2callback'))
    else:
        http_auth = credentials.authorize(httplib2.Http())
        plus_service = discovery.build('plus', 'v1', http_auth)
        me = plus_service.people().get(userId='me').execute()
        return render_template('cover.html', user=me)

@app.route('/oauth2callback')
def oauth2callback():
  flow = client.flow_from_clientsecrets(
      'client_secrets.json',
      scope='https://www.googleapis.com/auth/plus.me',
      redirect_uri=url_for('oauth2callback', _external=True, access_type="offline"))
  if 'code' not in request.args:
    auth_uri = flow.step1_get_authorize_url()
    return redirect(auth_uri)
  else:
    auth_code = request.args.get('code')
    credentials = flow.step2_exchange(auth_code)
    session['credentials'] = credentials.to_json()
    return redirect(url_for('index'))

@app.route('/create', methods=['POST'])
def createLetter():
    letter = Letter()
    letter.title = request.form.get('title')
    letter.text = request.form.get('text')
    letter.author = request.form.get('author')
    letter.location = ndb.GeoPt(float(request.form.get('lat')), float(request.form.get('lon')))
    key = letter.put()
    return json.dumps({'id': key.id()})

@app.route('/fetch')
def fetchLetter():
    letters = Letter.query().fetch()
    letters = map(lambda x: x.to_dict(), letters)
    return Response(json.dumps(letters, cls=TimeEncoder), status=200, mimetype='application/json')

@app.route('/logout')
def logout():
    session.pop('user_id', None)
    credentials = client.OAuth2Credentials.from_json(session['credentials'])
    credentials.revoke(httplib2.Http())
    session.clear()
    return redirect(url_for('comebacksoon'))

@app.route('/comebacksoon')
def comebacksoon():
    return render_template('comeback.html')

@app.errorhandler(404)
def page_not_found(e):
    """Return a custom 404 error."""
    return 'Sorry, Nothing at this URL.', 404


@app.errorhandler(500)
def application_error(e):
    """Return a custom 500 error."""
    return 'Sorry, unexpected error: {}'.format(e), 500

L.mapbox.accessToken = 'pk.<pk_key>';
var map = L.mapbox.map('map', 'mapbox.light').setView([40.4390456,-3.7437464], 9);

var position;

$( document ).ready(function() {
    console.log( "ready!" );
    $.get("fetch", function( data ) {
      buildMarkers(data);
    });
});

function buildMarkers(data){
  data.forEach(function(item){
    addMarker(item.location.lat, item.location.lon, item.title, item.text);
  });
}

function addMarker(lat, lon, title, text){
  var marker = L.marker([lat, lon], {
    icon: L.mapbox.marker.icon({
      'marker-size': 'large',
      'marker-symbol': 'post',
      'marker-color': '#2196F3'
    })
  });

  var popupContent =  '<h3>' + title + '</h3> <hr> <span>' + text + '</span>';
  marker.bindPopup(popupContent,{
      closeButton: true,
      minWidth: 320
  });
  marker.addTo(map);
}

$('#send-story-button').on('click', function (e) {
  // Get datastore_types
  var title = $('#letter-title').val();
  var text = $('#letter-text').val();

  // Clear input
  $('#letter-text').val('');
  $('#letter-title').val('');

  // Clear modal
  $('#writeModal').modal('toggle');

  // Form data and post
  var data = {'lon': position.lng, 'title': title, 'text': text, 'lat': position.lat, 'author': $('#name-text').text()};
  $.ajax({
    type: "POST",
    url: 'create',
    data: data,
    success: function(data){
      addMarker(position.lat, position.lng, title, text);
    }
  });
})

map.on('click', function(e) {
  position = e.latlng;
  $('#writeModal').modal('toggle');
});

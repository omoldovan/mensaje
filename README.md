# LETTER app

## Descripción
 Letter es una aplicación web, responsive, que permite la creación y la lectura de mensajes geolocalizados por parte de usuarios que se registran en la misma.

## Instrucciones para usuarios
 Abre la aplicación, haz login con tu cuenta de google +, navega por el mapamundi y busca donde quieres dejar un mensaje o simplemente fisgonea en los mensajes de otros.

## Instrucciones para replicar
 Copiar client_secrets.json en la carpeta raiz del proyecto. Este fichero se baja de la consola de GAE (Google App Engine) una vez creada una clave oAuth para la aplicación web que queramos usar para desplegar.
 Copiar la API_key de la cuenta de MapBox en la primera línea del fichero /statics/main.js 
 ```javascript
 L.mapbox.accessToken='<API_KEY>'
 ```
 
## Desarrollo
### Prerrequisitos
* Nota: Al poder usarse estos procedimientos en cualquier SO no se entra en detalle de los mismos.

Se puede usar cualquier IDE de edición de ficheros python (Pycharm, Atom etc.).
Se tiene que instalar la librería de Flask y demás dependencias (normalmente con pip)
Se tiene que instalar el SDK de GAE para python
Se necesita una cuenta en la plataforma web de MapBox ya que la API trabaja con api-keys
Se necesita una cuenta de GAE
Se tienen que descargar e instalar [1] y [2]


### Pasos
Para comenzar se descarga una estructura de proyecto [1]. En la misma carpeta desde un terminal o línea de comandos ejecutar:  pip install Flask -t lib para descargar sus dependencias. 
A partir de esta estructura escribiremos la transición entre páginas y crearemos las plantillas o el contenido de estas.
### Carpetas
Existen 3 carpetas: “lib” para albergar las librerías necesarias para la app, “static” para guardar los ficheros necesarios de la interfaz gráfica y otros .js y por último “templates” donde se encontrarán las plantillas para las páginas de la app.

### Bootstrap
Se utiliza para adornar la interfaz gráfica de la aplicación y hacer que la misma sea responsive.
### main.py
Este fichero albergará la orquestación de la app.
Para cumplir con todas las funcionalidades usa una serie de librerías:

```python
import json, httplib2, datetime, google
from oauth2client import client
from apiclient import discovery
from flask import Flask, render_template, redirect, url_for, session, request, Response
from google.appengine.ext import ndb
from time import mktime
```

Dentro del mismo se pueden crear clases útiles. En este caso se crea el modelo, class Letter y un parseador de datos JSON, class TimeEncoder.

Con ```@app.route('/')``` se indica que el método que la sigue es una llamada a una página con ese nombre (o a la raíz de la app).

Para el loggin y el logout se utilizan los métodos proporcionados por google [3] para autenticar o revocar el acceso.

Desde la pantalla principal se capturan los datos de los formularios de creación de texto que se parsean en el modelo para posteriormente mandarlas a la BBDD. La base de datos usada es la DataStore de GAE y se maneja con la librería ndb de google.appengine.ext.
También se recuperan los datos de la datastore (fetch) haciendo uso de una función lambda.
Como punto final se preparan 2 métodos para gestionar los típicos errores 404 y 500.

### main.js
 Este script js se utiliza para conectar con la API de MapBox [4] usando el accessToken.
A partir de aquí podemos recuperar un mapa centrado en el punto que queramos y con el nivel de zoom deseado.
Lo siguiente es recuperar la capa de datos que representan los mensajes llamando a la función fetch antes mencionada. Con cada objeto (mensaje y datos) recuperado se crea un marcador [5] que se posicionará en el mapa respetando su longitud y latitud.

## Templates
Flasck se encarga de completar las plantillas que escribamos aquí con código embebido y usando las referencias de los ficheros css y js.
Por ejemplo la siguiente línea indicará que se tiene que ejecutar el script main.js.

```javascript
<script src='{{ url_for('static',filename='main.js')}}'></script>
```

## Preparar la WebApp
Una vez creada una aplicación y unos credenciales[6] de tipo oAuth en el GAE se tiene que descargar un fichero .json con los datos del mismo “client_secrets.json” y guardarlo en la carpeta raíz del proyecto.
Si todo ha sido correcto podemos probar la aplicación en local ejecutando desde su directorio:
 dev_appserver.py .
Luego en un navegador podemos acceder a la misa a través de la dirección:
localhost:8080
Google proporciona una consola para manejar los servicios usados en local:
http://localhost:8000 
Para subir la app al servidor remoto tenemos que ejecutar:
 appcfg.py update  -V 1 --oauth2 -A letter-1326 .
donde el “.” representa la carpeta del proyecto y letter-1326 el nombre de la app remota.
Enlaces
# Ejemplo WebApp
http://letter-1326.appspot.com

# Referencias
[1] https://github.com/GoogleCloudPlatform/appengine-python-flask-skeleton.git
[2] https://cloud.google.com/appengine/downloads#Google_App_Engine_SDK_for_Python
[3] https://developers.google.com/api-client-library/python/auth/web-app#creatingcred
[4] https://www.mapbox.com/mapbox.js/api/v2.1.9/api-access-tokens/
[5] https://www.mapbox.com/mapbox.js/api/v2.1.9/l-mapbox-marker-icon/
[6] https://developers.google.com/api-client-library/python/auth/web-app#creatingcred